package service

import (
	"context"
	"reflect"
	"testing"
	"time"

	"github.com/golang/mock/gomock"

	"gitlab.com/zapirus/geotask/geo"
	"gitlab.com/zapirus/geotask/module/order/models"
	"gitlab.com/zapirus/geotask/module/order/storage"
	mock_storage "gitlab.com/zapirus/geotask/module/order/storage/mock"
)

var (
	allowed  = geo.NewAllowedZone()
	disabled = []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()}
)

func TestOrderService_GenerateOrder(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockOrderStorager := mock_storage.NewMockOrderStorager(ctrl)
	mockOrderStorager.EXPECT().GenerateUniqueID(gomock.Any()).Return(int64(0), nil)
	mockOrderStorager.EXPECT().Save(gomock.Any(), gomock.Any(), gomock.Any()).Return(nil)

	type fields struct {
		storage       storage.OrderStorager
		allowedZone   geo.PolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "generate order",
			fields: fields{
				storage:       mockOrderStorager,
				allowedZone:   allowed,
				disabledZones: disabled,
			},
			args:    args{ctx: context.Background()},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &OrderService{
				storage:       tt.fields.storage,
				allowedZone:   tt.fields.allowedZone,
				disabledZones: tt.fields.disabledZones,
			}
			if err := o.GenerateOrder(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("GenerateOrder() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestOrderService_GetByRadius(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockOrderStorager := mock_storage.NewMockOrderStorager(ctrl)

	orders := []models.Order{
		{
			ID:            1,
			Price:         999,
			DeliveryPrice: 300,
			Lng:           12.3456,
			Lat:           23.3456,
			IsDelivered:   false,
			CreatedAt:     time.Now(),
		},
	}

	mockOrderStorager.EXPECT().GetByRadius(context.TODO(), gomock.Any(), gomock.Any(), 5.0, gomock.Any()).Return(orders, nil)

	type fields struct {
		storage       storage.OrderStorager
		allowedZone   geo.PolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx    context.Context
		lng    float64
		lat    float64
		radius float64
		unit   string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []models.Order
		wantErr bool
	}{
		{
			name: "get by radius",
			fields: fields{
				storage:       mockOrderStorager,
				allowedZone:   allowed,
				disabledZones: disabled,
			},
			args: args{
				ctx:    context.TODO(),
				lng:    12.3456,
				lat:    23.3456,
				radius: 5.0,
				unit:   "",
			},
			want:    orders,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &OrderService{
				storage:       tt.fields.storage,
				allowedZone:   tt.fields.allowedZone,
				disabledZones: tt.fields.disabledZones,
			}
			got, err := o.GetByRadius(tt.args.ctx, tt.args.lng, tt.args.lat, tt.args.radius, tt.args.unit)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByRadius() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByRadius() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOrderService_GetCount(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mock := mock_storage.NewMockOrderStorager(ctrl)
	mock.EXPECT().GetCount(context.TODO()).Return(1, nil)

	type fields struct {
		storage       storage.OrderStorager
		allowedZone   geo.PolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		{
			name: "get count",
			fields: fields{
				storage:       mock,
				allowedZone:   allowed,
				disabledZones: disabled,
			},
			args:    args{ctx: context.TODO()},
			want:    1,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &OrderService{
				storage:       tt.fields.storage,
				allowedZone:   tt.fields.allowedZone,
				disabledZones: tt.fields.disabledZones,
			}
			got, err := o.GetCount(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetCount() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetCount() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOrderService_RemoveOldOrders(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mock := mock_storage.NewMockOrderStorager(ctrl)
	mock.EXPECT().RemoveOldOrders(gomock.Any(), gomock.Any()).Return(nil)
	type fields struct {
		storage       storage.OrderStorager
		allowedZone   geo.PolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "remove old orders",
			fields: fields{
				storage:       mock,
				allowedZone:   allowed,
				disabledZones: disabled,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &OrderService{
				storage:       tt.fields.storage,
				allowedZone:   tt.fields.allowedZone,
				disabledZones: tt.fields.disabledZones,
			}
			if err := o.RemoveOldOrders(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("RemoveOldOrders() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestOrderService_Save(t *testing.T) {

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mock := mock_storage.NewMockOrderStorager(ctrl)
	mock.EXPECT().Save(gomock.Any(), gomock.Any(), gomock.Any()).Return(nil)

	type fields struct {
		storage       storage.OrderStorager
		allowedZone   geo.PolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx   context.Context
		order models.Order
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "save",
			fields: fields{
				storage:       mock,
				allowedZone:   allowed,
				disabledZones: disabled,
			},
			args: args{
				ctx:   context.TODO(),
				order: models.Order{},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &OrderService{
				storage:       tt.fields.storage,
				allowedZone:   tt.fields.allowedZone,
				disabledZones: tt.fields.disabledZones,
			}
			if err := o.Save(tt.args.ctx, tt.args.order); (err != nil) != tt.wantErr {
				t.Errorf("Save() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
