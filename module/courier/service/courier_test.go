package service

import (
	"context"
	"reflect"
	"testing"

	"github.com/golang/mock/gomock"

	"gitlab.com/zapirus/geotask/geo"
	"gitlab.com/zapirus/geotask/module/courier/models"
	"gitlab.com/zapirus/geotask/module/courier/storage"
	mock "gitlab.com/zapirus/geotask/module/courier/storage/mock"
)

var (
	allowed  = geo.NewAllowedZone()
	disabled = []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()}
)

func TestCourierService_GetCourier(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockController := mock.NewMockCourierStorager(ctrl)
	courier := models.Courier{
		Score: 0,
		Location: models.Point{
			Lat: DefaultCourierLat,
			Lng: DefaultCourierLng,
		},
	}

	mockController.EXPECT().GetOne(context.TODO()).Return(nil, nil)
	mockController.EXPECT().Save(context.TODO(), courier).Return(nil)

	type fields struct {
		courierStorage storage.CourierStorager
		allowedZone    geo.PolygonChecker
		disabledZones  []geo.PolygonChecker
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *models.Courier
		wantErr bool
	}{
		{
			name: "base test 1",
			fields: fields{
				courierStorage: mockController,
				allowedZone:    allowed,
				disabledZones:  disabled,
			},
			args:    args{context.TODO()},
			want:    &courier,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &CourierService{
				courierStorage: tt.fields.courierStorage,
				allowedZone:    tt.fields.allowedZone,
				disabledZones:  tt.fields.disabledZones,
			}
			got, err := c.GetCourier(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetCourier() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetCourier() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCourierService_MoveCourier(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockController := mock.NewMockCourierStorager(ctrl)
	courier := models.Courier{
		Score: 0,
		Location: models.Point{
			Lat: DefaultCourierLat,
			Lng: DefaultCourierLng,
		},
	}

	mockController.EXPECT().Save(context.Background(), courier).Return(nil)

	type fields struct {
		courierStorage storage.CourierStorager
		allowedZone    geo.PolygonChecker
		disabledZones  []geo.PolygonChecker
	}
	type args struct {
		courier   models.Courier
		direction int
		zoom      int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "base test 2",
			fields: fields{
				courierStorage: mockController,
				allowedZone:    allowed,
				disabledZones:  disabled,
			},
			args: args{
				courier:   courier,
				direction: DirectionRight,
				zoom:      200,
			},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &CourierService{
				courierStorage: tt.fields.courierStorage,
				allowedZone:    tt.fields.allowedZone,
				disabledZones:  tt.fields.disabledZones,
			}
			if err := c.MoveCourier(tt.args.courier, tt.args.direction, tt.args.zoom); (err != nil) != tt.wantErr {
				t.Errorf("MoveCourier() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
