package storage
//go:generate mockgen -destination=/home/huawei/Документы/golang/geotask/module/courier/storage/mock/courier_mock.go -source=/home/huawei/Документы/golang/geotask/module/courier/storage/courier_storage.go -package=storage
import (
	"context"
	"encoding/json"
	"log"

	"github.com/go-redis/redis/v8"

	"gitlab.com/zapirus/geotask/module/courier/models"
)

type CourierStorager interface {
	Save(ctx context.Context, courier models.Courier) error // сохранить курьера по ключу courier
	GetOne(ctx context.Context) (*models.Courier, error)    // получить курьера по ключу courier
}

type CourierStorage struct {
	storage *redis.Client
}

func (c *CourierStorage) Save(ctx context.Context, courier models.Courier) error {
	obj, err := json.Marshal(courier)
	if err != nil {
		return err
	}
	err = c.storage.Set(ctx, "courier", obj, 0).Err()
	if err != nil {
		return err
	}

	return nil
}

func (c *CourierStorage) GetOne(ctx context.Context) (*models.Courier, error) {
	courier := models.Courier{}
	data, err := c.storage.Get(ctx, "courier").Bytes()
	if err != nil {
		if err == redis.Nil {
			log.Println("RETURNING nil nil")
			return nil, nil
		}
		return nil, err
	}

	err = json.Unmarshal(data, &courier)
	if err != nil {
		log.Println("Problems while unmarshal")
		return nil, err
	}

	return &courier, nil
}

func NewCourierStorage(storage *redis.Client) CourierStorager {
	return &CourierStorage{storage: storage}
}
