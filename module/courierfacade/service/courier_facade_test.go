package service

import (
	"context"
	"reflect"
	"testing"
	"time"

	"github.com/golang/mock/gomock"

	"gitlab.com/zapirus/geotask/geo"
	models_courier "gitlab.com/zapirus/geotask/module/courier/models"
	"gitlab.com/zapirus/geotask/module/courier/service"
	mock_courier "gitlab.com/zapirus/geotask/module/courier/storage/mock"
	"gitlab.com/zapirus/geotask/module/courierfacade/models"
	models_order "gitlab.com/zapirus/geotask/module/order/models"
	service2 "gitlab.com/zapirus/geotask/module/order/service"
	mock_order "gitlab.com/zapirus/geotask/module/order/storage/mock"
)

var (
	allowed  = geo.NewAllowedZone()
	disabled = []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()}
)

func TestCourierFacade_GetStatus(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	courier := models_courier.Courier{
		Score: 0,
		Location: models_courier.Point{
			59.9311,
			30.3609,
		},
	}
	orders := []models_order.Order{{
		ID:            1,
		Price:         999,
		DeliveryPrice: 300,
		Lng:           12.3456,
		Lat:           23.3456,
		IsDelivered:   false,
		CreatedAt:     time.Now(),
	},
	}

	courierMock := mock_courier.NewMockCourierStorager(ctrl)
	orderMock := mock_order.NewMockOrderStorager(ctrl)

	courierService := service.NewCourierService(courierMock, allowed, disabled)
	orderService := service2.NewOrderService(orderMock, allowed, disabled)

	courierMock.EXPECT().GetOne(context.Background()).Return(nil, nil)
	courierMock.EXPECT().Save(gomock.Any(), courier).Return(nil)
	orderMock.EXPECT().GetByRadius(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any(), "m").Return(orders, nil)

	type fields struct {
		courierService service.Courierer
		orderService   service2.Orderer
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   models.CourierStatus
	}{
		{
			name: "get status",
			fields: fields{
				courierService: courierService,
				orderService:   orderService,
			},
			args: args{ctx: context.TODO()},
			want: models.CourierStatus{
				Courier: courier,
				Orders:  orders,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &CourierFacade{
				courierService: tt.fields.courierService,
				orderService:   tt.fields.orderService,
			}
			if got := c.GetStatus(tt.args.ctx); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetStatus() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCourierFacade_MoveCourier(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	courier := models_courier.Courier{
		Score: 0,
		Location: models_courier.Point{
			59.9311,
			30.3609,
		},
	}

	courierMock := mock_courier.NewMockCourierStorager(ctrl)
	orderMock := mock_order.NewMockOrderStorager(ctrl)

	courierService := service.NewCourierService(courierMock, allowed, disabled)
	orderService := service2.NewOrderService(orderMock, allowed, disabled)

	courierMock.EXPECT().GetOne(gomock.Any()).Return(nil, nil)
	courierMock.EXPECT().Save(gomock.Any(), courier).Return(nil)
	courierMock.EXPECT().Save(gomock.Any(), courier).Return(nil)

	type fields struct {
		courierService service.Courierer
		orderService   service2.Orderer
	}
	type args struct {
		ctx       context.Context
		direction int
		zoom      int
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "move",
			fields: fields{
				courierService: courierService,
				orderService:   orderService,
			},
			args: args{
				ctx:       context.TODO(),
				direction: 2,
				zoom:      300,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &CourierFacade{
				courierService: tt.fields.courierService,
				orderService:   tt.fields.orderService,
			}
			c.MoveCourier(tt.args.ctx, tt.args.direction, tt.args.zoom)
		})
	}
}
