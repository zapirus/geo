package service
//go:generate mockgen -destination=/home/huawei/Документы/golang/geotask/module/courierfacade/service/mock/facade_mock.go -source=/home/huawei/Документы/golang/geotask/module/courierfacade/service/courier_facade.go -package=api
import (
	"context"
	"log"

	cservice "gitlab.com/zapirus/geotask/module/courier/service"
	cfm "gitlab.com/zapirus/geotask/module/courierfacade/models"
	oservice "gitlab.com/zapirus/geotask/module/order/service"
)

const (
	CourierVisibilityRadius = 2800 // 2.8km
)

type CourierFacer interface {
	MoveCourier(ctx context.Context, direction, zoom int) // отвечает за движение курьера по карте direction - направление движения, zoom - уровень зума
	GetStatus(ctx context.Context) cfm.CourierStatus      // отвечает за получение статуса курьера и заказов вокруг него
}

// CourierFacade фасад для курьера и заказов вокруг него (для фронта)
type CourierFacade struct {
	courierService cservice.Courierer
	orderService   oservice.Orderer
}

func (c *CourierFacade) MoveCourier(ctx context.Context, direction, zoom int) {
	courier, err := c.courierService.GetCourier(ctx)
	if err != nil {
		log.Println("Problems while moving : ", err)
	}

	err = c.courierService.MoveCourier(*courier, direction, zoom)
	if err != nil {
		log.Println("Problems while moving : ", err)
	}
}

func (c *CourierFacade) GetStatus(ctx context.Context) cfm.CourierStatus {
	courier, err := c.courierService.GetCourier(ctx)
	if err != nil {
		log.Println("Problems while getting status : ", err)
		return cfm.CourierStatus{}
	}
	log.Println("Courier is", courier)

	orders, err := c.orderService.GetByRadius(ctx, courier.Location.Lng, courier.Location.Lat, CourierVisibilityRadius, "m")
	if err != nil {
		log.Println("ERR WHILE GETTING BY RADIUS")
		return cfm.CourierStatus{}
	}
	log.Println("Orders are", orders)
	return cfm.CourierStatus{
		Courier: *courier,
		Orders:  orders,
	}

}
func NewCourierFacade(courierService cservice.Courierer, orderService oservice.Orderer) CourierFacer {
	return &CourierFacade{courierService: courierService, orderService: orderService}
}
