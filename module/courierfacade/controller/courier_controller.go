package controller

import (
	"context"
	"encoding/json"
	"time"

	"github.com/gin-gonic/gin"

	"gitlab.com/zapirus/geotask/module/courierfacade/service"
)

type CourierController struct {
	courierService service.CourierFacer
}

func NewCourierController(courierService service.CourierFacer) *CourierController {
	return &CourierController{courierService: courierService}
}

func (c *CourierController) GetStatus(ctx *gin.Context) {
	// установить задержку в 50 миллисекунд
	time.Sleep(time.Millisecond * 50)
	// получить статус курьера из сервиса courierService используя метод GetStatus
	status := c.courierService.GetStatus(ctx)
	//log.Println("STATUS IS", status.Courier, status.Orders)
	// отправить статус курьера в ответ
	ctx.JSON(200, status)
}

func (c *CourierController) MoveCourier(m webSocketMessage) {
	var cm CourierMove
	var err error
	// получить данные из m.Data и десериализовать их в структуру CourierMove
	data := m.Data.([]byte)
	err = json.Unmarshal(data, &cm)
	if err != nil {
		return
	}
	// вызвать метод MoveCourier у courierService
	c.courierService.MoveCourier(context.Background(), cm.Direction, cm.Zoom)
}
